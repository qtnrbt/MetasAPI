var express = require('express');

var cookieParser = require('cookie-parser');
var logger = require('morgan');

const cors = require("cors");

var getMetas = require('./routes/get-metas.js');

var app = express();

app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());



app.use('/', getMetas);

module.exports = app;
