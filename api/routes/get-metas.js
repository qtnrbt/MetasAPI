var express = require('express');
var app = express.Router();

const cheerio = require("cheerio");
const axios = require("axios");

app.get("/", (req, res) => {

    if(!req.query.url) return res.status(500).send({msg: "Please add the required url query parameter such as https://api.quentinrobert.fr/metas?url=theUrlYouWant"});

    const metas = async () => {

        const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
        const regex = new RegExp(expression);

        if(!req.query.url.match(regex)) throw {msg: "Wrong url type : it should be starting by http:// or https://"};

        const response = await axios.get(req.query.url, {headers: {'User-Agent': 'PostmanRuntime/7.28.4' }});
        const html = response.data;

        const $ = cheerio.load(html);

        return new Promise((resolve) => {
            
            let imageLink;
            let name;
            let site = null;
            let description;

            $("meta").each((id, meta) => {

                // Get title
                if(meta.attribs.property == "title" || meta.attribs.name == "title") {
                    name = meta.attribs.content.toString();
                }
                if(meta.attribs.property == "twitter:title" || meta.attribs.name == "twitter:title") {
                    name = meta.attribs.content.toString();
                }

                // Get site name
                if(meta.attribs.property == "application-name" || meta.attribs.name == "application-name") {
                    site = meta.attribs.content.toString();
                }
                if((meta.attribs.property == "og:site_name" || meta.attribs.name == "og:site_name") && !site) {
                    site = meta.attribs.content.toString();
                }
                if((meta.attribs.property == "author" || meta.attribs.name == "author") && !site) {
                    site = meta.attribs.content.toString();
                }
                if((meta.attribs.property == "twitter:site" || meta.attribs.name == "twitter:site") && !site) {
                    site = meta.attribs.content.toString();
                }
                
                // Get image
                if(meta.attribs.property == "og:image" || meta.attribs.name == "og:image") {
                    imageLink = meta.attribs.content.toString();
                }
                if((meta.attribs.property == "twitter:image" || meta.attribs.name == "twitter:image") && !imageLink) {
                    imageLink = meta.attribs.content.toString();
                }

                // Get description
                if(meta.attribs.property == "description" || meta.attribs.name == "description") {
                    description = meta.attribs.content.toString();
                }
                if((meta.attribs.property == "og:description" || meta.attribs.name == "og:description") && !description) {
                    description = meta.attribs.content.toString();
                }

            })

            imageLink = imageLink || "https://cdn.quentinrobert.fr/metas/image-not-found.jpeg";
            site = site || "the web"
            return resolve({name, imageLink, site, description});

        })
    }

    metas()
    .then(response => {
        return res.status(200).send(response);
    })
    .catch(err => {
        return res.status(401).send(err);
    })



})

module.exports = app;
